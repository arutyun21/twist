#include <twist/threading/stdlike.hpp>

namespace twist {
namespace th {

#if defined(TWIST_FIBER)

#pragma message("Threading backend: fibers")

const thread_id kInvalidThreadId = -1;

#else

#pragma message("Threading backend: threads")

const std::thread::id kInvalidThreadId{};

#endif

}  // namespace th
}  // namespace twist
