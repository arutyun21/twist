#pragma once

#include <twist/fault/adversary.hpp>

namespace twist {
namespace fault {

IAdversaryPtr CreateLockFreeAdversary();

}  // namespace fault
}  // namespace twist
