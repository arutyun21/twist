#include <twist/fault/nop.hpp>

namespace twist {
namespace fault {

/////////////////////////////////////////////////////////////////////

class NopAdversary : public IAdversary {
 public:
  void Reset() override {
  }

  void PrintReport() override {
  }

  // per-thread methods

  void Enter() override {
    // do nothing
  }

  void Fault() override {
    // do nothing
  }

  void ReportProgress() override {
    // ignore lock-free algorithms
  }

  void Exit() override {
    // do nothing
  }
};

/////////////////////////////////////////////////////////////////////

IAdversaryPtr CreateNopAdversary() {
  return std::make_shared<NopAdversary>();
}

}  // namespace fault
}  // namespace twist
