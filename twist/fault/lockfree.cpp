#include <twist/fault/lockfree.hpp>

#include <twist/fault/yielder.hpp>

#include <twist/threading/stdlike.hpp>
#include <twist/threading/tls.hpp>

#include <twist/logging/logging.hpp>

#include <twist/support/compiler.hpp>
#include <twist/support/intrusive_list.hpp>
#include <twist/support/locking.hpp>
#include <twist/support/random.hpp>

#include <set>

// TODO(Lipovsky): Separate implementation for fibers

namespace twist {
namespace fault {

template <typename T>
class ThreadLocal {
  struct Node {
    T value_;
  };

 public:
  ThreadLocal()
      : tls_key_(twist::th::AcquireKey()) {
  }

  T& Access() {
    Node* node = AccessNode();
    return node->value_;
  }

  void Cleanup() {
    CleanupNode();
  }

  ~ThreadLocal() {
    twist::th::ReleaseKey(tls_key_);
  }

 private:
  Node* AccessNode() {
    Node** node = AccessSlot();
    if (*node == nullptr) {
      *node = new Node();
    }
    return *node;
  }

  void CleanupNode() {
    Node** node = AccessSlot();
    if (*node != nullptr) {
      delete *node;
      *node = nullptr;
    }
  }

  Node** AccessSlot() {
    return (Node**)twist::th::AccessSlot(tls_key_);
  }

 private:
  twist::th::Key tls_key_;
};

/////////////////////////////////////////////////////////////////////

class OneShotEvent {
 public:
  void Set() {
    auto lock = LockUnique(mutex_);
    ready_ = true;
    ready_cv_.notify_one();
  }

  void Await() {
    auto lock = LockUnique(mutex_);
    while (!ready_) {
      ready_cv_.wait(lock);
    }
  }

 private:
  twist::th::mutex mutex_;
  twist::th::condition_variable ready_cv_;
  bool ready_{false};
};


// Not thread safe
class ThreadWaitQueue {
 public:
  struct WaitNode : public IntrusiveListNode<WaitNode>,
                    public OneShotEvent {
    void Wake() {
      Set();
    }
  };

  using WaitQueue = IntrusiveList<WaitNode>;

 public:
  // Suspend current thread
  void Add(WaitNode* this_thread_node) {
    wait_queue_.PushBack(this_thread_node);
  }

  void ResumeRandomThread() {
    if (!wait_queue_.IsEmpty()) {
      auto* wait_node = UnlinkRandomItem(wait_queue_);
      wait_node->Wake();
    }
  }

 private:
  WaitQueue wait_queue_;
};

/////////////////////////////////////////////////////////////////////

class ThreadInfo {
 public:
  bool IsRegistered() const {
    return registered_;
  }

  void Register() {
    registered_ = true;
  }

 private:
  bool registered_ = false;
};

class LockFreeAdversary : public IAdversary {
 public:
  LockFreeAdversary(size_t suspend_freq, size_t suspended_thread_limit, size_t resume_freq,
                    size_t yield_freq)
      : suspended_thread_limit_(suspended_thread_limit),
        suspend_(suspend_freq),
        resume_(resume_freq),
        yielder_(yield_freq) {
  }

  void Reset() override {
    suspend_.Reset();
    resume_.Reset();
    yielder_.Reset();
  }

  void PrintReport() override {
    VERIFY(suspended_thread_count_ == 0, "some threads still suspended");
  }

  // Per-thread methods

  void Enter() override {
    // TODO(Lipovsky): check thread ids
    ++thread_count_;

    // This thread can be parked
    auto& self = threads_.Access();
    self.Register();
  }

  void Fault() override {
    size_t inject_count = fault_count_.fetch_add(1, std::memory_order_relaxed);

    if (suspend_.Test()) {
      SuspendThisThread(inject_count);
    } else {
      yielder_.MaybeYield();
    }
  }

  void ReportProgress() override {
    // Wait-free on fast path, locking on slow path
    if (suspended_thread_count_ > 0 && resume_.Test()) {
      ResumeThread();
    }
  }

  void Exit() override {
    size_t threads_left = thread_count_--;

    threads_.Cleanup();

    UNUSED(threads_left);
    //LOG_SIMPLE("Thread exit (left: " << threads_left << ")");

    // At least one running thread!
    ResumeThread();
  }

 private:
  void ResumeThread() {
    auto lock = twist::LockUnique(mutex_);
    suspended_threads_.ResumeRandomThread();
  }

  void SuspendThisThread(size_t start_fault_count) {
    auto self = threads_.Access();
    if (!self.IsRegistered()) {
      return;
    }

    ThreadWaitQueue::WaitNode this_thread_node;

    {
      auto lock = twist::LockUnique(mutex_);

      if (suspended_thread_count_ >= suspended_thread_limit_) {
        // Too many suspended threads
        return;
      }

      if (suspended_thread_count_ + 1 >= thread_count_) {
        // At least one thread should run
        return;
      }

      suspended_threads_.Add(&this_thread_node);
      ++suspended_thread_count_;
      //LOG_SIMPLE("Thread " << twist::th::this_thread::get_id() << " suspended by adversary (suspended: " << suspended_thread_count_ << ")");
    }

    this_thread_node.Await();

    --suspended_thread_count_;

    size_t suspend_time = fault_count_ - start_fault_count;
    UNUSED(suspend_time);
    // LOG_SIMPLE("Thread resumed by adversary, suspend 'time': "
    //               << suspend_time << " (suspended: " << suspended_thread_count_
    //               << ")");
  }

 private:
  std::atomic<size_t> fault_count_{0};

  std::atomic<size_t> thread_count_{0};

  size_t suspended_thread_limit_;

  ThreadLocal<ThreadInfo> threads_;

  RandomEvent suspend_;
  RandomEvent resume_;

  twist::th::mutex mutex_;
  ThreadWaitQueue suspended_threads_;
  std::atomic<size_t> suspended_thread_count_{0};

  Yielder yielder_;
};

/////////////////////////////////////////////////////////////////////

IAdversaryPtr CreateLockFreeAdversary() {
  return std::make_shared<LockFreeAdversary>(10, 2, 5, 10);
}

}  // namespace fault
}  // namespace twist
