#include <twist/fault/adversary.hpp>

#include <twist/fault/yielder.hpp>

#include <twist/support/assert.hpp>
#include <twist/support/singleton.hpp>
#include <twist/support/sync_output.hpp>
#include <twist/logging/logging.hpp>

#include <atomic>
#include <cstdint>
#include <iostream>
#include <thread>

namespace twist {
namespace fault {

/////////////////////////////////////////////////////////////////////

class YieldAdversary : public IAdversary {
 public:
  YieldAdversary(size_t yield_freq) : yielder_(yield_freq) {
  }

  // per-test methods

  void Reset() override {
    yielder_.Reset();
  }

  void PrintReport() override {
    SyncCout() << "Context switches injected: " << yielder_.YieldCount()
               << std::endl;
  }

  // per-thread methods

  void Enter() override {
    // do nothing
  }

  void Fault() override {
    yielder_.MaybeYield();
  }

  void ReportProgress() override {
    // ignore lock-free algorithms
  }

  void Exit() override {
    // do nothing
  }

 private:
  Yielder yielder_;
};

/////////////////////////////////////////////////////////////////////

class Holder {
 public:
  Holder() : adversary_(CreateDefaultAdversary()) {
  }

  IAdversary* Get() {
    return adversary_.get();
  }

  void Set(IAdversaryPtr adversary) {
    adversary_ = std::move(adversary);
  }

 private:
  static IAdversaryPtr CreateDefaultAdversary() {
    return std::make_shared<YieldAdversary>(10);
  }

 private:
  IAdversaryPtr adversary_;
};

IAdversary* GetAdversary() {
  return Singleton<Holder>()->Get();
}

void SetAdversary(IAdversaryPtr adversary) {
  Singleton<Holder>()->Set(std::move(adversary));
}

void AccessAdversary() {
  (void)GetAdversary();
}

}  // namespace fault
}  // namespace twist
