#pragma once

#include <twist/fault/adversary.hpp>

namespace twist {
namespace fault {

IAdversaryPtr CreateNopAdversary();

}  // namespace fault
}  // namespace twist
