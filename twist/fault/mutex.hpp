#pragma once

#include <twist/threading/stdlike.hpp>

namespace twist {
namespace fault {

class FaultyMutex {
 public:
  FaultyMutex();

  // std::mutex-like / Lockable
  void lock();      // NOLINT
  bool try_lock();  // NOLINT
  void unlock();    // NOLINT

 private:
  twist::th::mutex impl_;
  twist::th::thread_id owner_id_;
};

}  // namespace fault
}  // namespace twist
