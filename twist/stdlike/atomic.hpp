#pragma once

#if defined(TWIST_FAULTY)

#include <twist/fault/atomic.hpp>

namespace twist {

template <typename T>
using atomic = fault::FaultyAtomic<T>;  // NOLINT
}  // namespace twist

#else

#include <atomic>

namespace twist {
using std::atomic;
}  // namespace twist

#endif
