#pragma once

#include <twist/threading/stdlike.hpp>

namespace twist {

// https://en.cppreference.com/w/cpp/thread/thread
using thread = th::thread;  // NOLINT

namespace this_thread {

// https://en.cppreference.com/w/cpp/thread/yield
inline void yield() {  // NOLINT
  th::this_thread::yield();
}

// https://en.cppreference.com/w/cpp/thread/sleep_for
inline void sleep_for(std::chrono::nanoseconds delay) {  // NOLINT
  th::this_thread::sleep_for(delay);
}

}  // namespace this_thread

}  // namespace twist
