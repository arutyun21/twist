#pragma once

#include <memory>
#include <string>

////////////////////////////////////////////////////////////////////////////////

class ITest {
 public:
  virtual ~ITest() = default;

  virtual void Run() = 0;
  virtual std::string Name() const = 0;
  virtual std::string Describe() const = 0;
};

using ITestPtr = std::shared_ptr<ITest>;
