#include <twist/test_utils/executor.hpp>

namespace twist {

void ScopedExecutor::Submit(Task task) {
  auto runner = [task]() { Run(task); };
  threads_.emplace_back(runner);
}

void ScopedExecutor::Join() {
  if (joined_) {
    return;
  }
  for (auto& t : threads_) {
    t.join();
  }
  joined_ = true;
}

void ScopedExecutor::Run(Task task) {
  SetTestThreadAffinity();

  try {
    task();
  } catch (...) {
    FailTestByException();
  }
}

}  // namespace twist
