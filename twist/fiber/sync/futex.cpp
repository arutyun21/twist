#include <twist/support/singleton.hpp>
#include <twist/fiber/core/wait_queue.hpp>

#include <map>

namespace twist {
namespace fiber {

using UIntAddr = unsigned int*;

class FutexRegistry {
 public:
  int Wait(UIntAddr addr, int value) {
    if (*addr == (unsigned int)value) {
      auto& wait_queue = AddrWaitQueue(addr);
      wait_queue.Park();
      return 0;
    }
    return -1;  // ~ EAGAIN
  }

  int Wake(UIntAddr addr, int count) {
    auto& wait_queue = AddrWaitQueue(addr);
    return (size_t)wait_queue.Wake((size_t)count);
  }

 private:
  WaitQueue& AddrWaitQueue(UIntAddr addr) {
    return queues_[addr];
  }

 private:
  std::map<UIntAddr, WaitQueue> queues_;
};

int FutexWait(UIntAddr addr, int value) {
  return Singleton<FutexRegistry>()->Wait(addr, value);
}

int FutexWake(UIntAddr addr, int count) {
  return Singleton<FutexRegistry>()->Wake(addr, count);
}

}  // namespace fiber
}  // namespace twist
