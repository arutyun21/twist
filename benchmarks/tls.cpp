#include <benchmark/benchmark.h>

#include <twist/fiber/core/scheduler.hpp>
#include <twist/fiber/core/fls.hpp>
#include <twist/thread/tls.hpp>

static void BM_FiberAccessFls(benchmark::State& state) {
  auto main = [&]() {
    auto key = twist::fiber::fls::AcquireKey();
    for (auto _ : state) {
      void* slot = twist::fiber::fls::AccessSlot(key);
      *(int*)slot = 42;
    }
  };

  twist::fiber::RunScheduler(main);
}

BENCHMARK(BM_FiberAccessFls);

static void BM_ThreadAccessTls(benchmark::State& state) {
  auto key = twist::thread::tls::AcquireKey();
  for (auto _ : state) {
    void* slot = twist::thread::tls::AccessSlot(key);
    *(int*)slot = 42;
  }
}

BENCHMARK(BM_ThreadAccessTls);

BENCHMARK_MAIN();

