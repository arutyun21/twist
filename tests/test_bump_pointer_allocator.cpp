#include <twist/memory/bump_pointer_allocator.hpp>

#include <twist/test_utils/barrier.hpp>
#include <twist/test_framework/test_framework.hpp>

#include <mutex>
#include <thread>

TEST_SUITE(BumpPointerAllocator) {
  SIMPLE_TEST(AllocateInts) {
    const size_t kObjects = 256;
    const size_t kArenaSize = 1024 * 1024;

    twist::BumpPointerAllocator allocator{kArenaSize};

    for (size_t i = 0; i < kObjects; ++i) {
      uint32_t* ptr = allocator.New<uint32_t>(i);
      ASSERT_EQ(*ptr, i);
    }
  }

  struct TestObject {
    static size_t ctor_count_;
    static size_t dtor_count_;

    size_t index_;

    TestObject(size_t index) : index_(index) {
      ++ctor_count_;
    }

    static void ResetCounters() {
      ctor_count_ = 0;
      dtor_count_ = 0;
    }

    ~TestObject() {
      ++dtor_count_;
    }
  };

  size_t TestObject::ctor_count_{0};
  size_t TestObject::dtor_count_{0};

  SIMPLE_TEST(CheckDestructorsCalled) {
    const size_t kObjects = 10000;
    const size_t kArenaSize = 1024 * 1024;

    twist::BumpPointerAllocator allocator{kArenaSize};

    TestObject::ResetCounters();
    for (size_t i = 0; i < kObjects; ++i) {
      TestObject* object = allocator.New<TestObject>(i);
      ASSERT_EQ(object->index_, i);
    }

    allocator.Reset();

    ASSERT_EQ(TestObject::ctor_count_, kObjects);
    ASSERT_EQ(TestObject::dtor_count_, kObjects);
  }

  SIMPLE_TEST(MixedTypes) {
    static const size_t kArenaSize = 64 * 1024;
    twist::BumpPointerAllocator allocator{kArenaSize};

    int* int_ptr = allocator.New<int>(123);

    std::string* string_ptr = allocator.New<std::string>("hello!");

    std::vector<int>* vector_ptr = allocator.New<std::vector<int>>();
    vector_ptr->push_back(1);
    vector_ptr->push_back(2);
    vector_ptr->push_back(3);

    ASSERT_EQ(*int_ptr, 123);
    ASSERT_EQ(*string_ptr, "hello!");
    ASSERT_EQ(vector_ptr->size(), 3);
  }

  SIMPLE_TEST(AllocateStringsConcurrently) {
    const size_t kThreads = 10;
    const size_t kObjectsPerThread = 1000;
    const size_t kArenaSize = 1024 * 1024;;

    twist::BumpPointerAllocator allocator{kArenaSize};

    twist::OnePassBarrier start_barrier{kThreads};

    auto routine = [&]() {
      start_barrier.PassThrough();

      for (size_t i = 0; i < kObjectsPerThread; ++i) {
        std::string* string = allocator.New<std::string>("hello, world!");
        ASSERT_EQ(*string, "hello, world!");
      }
    };

    std::vector<std::thread> threads;
    for (size_t t = 0; t < kThreads; ++t) {
      threads.emplace_back(routine);
    }
    for (auto& thread : threads) {
      thread.join();
    }
  }
}
