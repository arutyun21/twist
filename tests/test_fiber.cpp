#include <twist/fiber/core/scheduler.hpp>
#include <twist/fiber/core/wait_queue.hpp>
#include <twist/fiber/core/fls.hpp>

#include <twist/fiber/sync/mutex.hpp>
#include <twist/fiber/sync/thread.hpp>
#include <twist/fiber/sync/futex.hpp>

#include <twist/test_framework/test_framework.hpp>

#include <twist/logging/logging.hpp>

using namespace twist::fiber;

TEST_SUITE(Fiber) {
  SIMPLE_TEST(OneFiber) {
    RunScheduler([]() {
      LOG_SIMPLE("write to log from fiber!");
    });
  }

  struct TestException {
  };

  SIMPLE_TEST(Exceptions) {
    auto bar = []() {
      throw TestException();
    };

    auto foo = [&]() {
      bar();
    };

    auto main = [&]() {
      ASSERT_THROW(foo(), TestException);
    };

    RunScheduler(main);
  }

  SIMPLE_TEST(Fuel) {
    size_t count = 0;

    auto loop = [&]() {
      while (true) {
        ++count;
        Yield();
      }
    };

    static const size_t kFuel = 123;

    RunScheduler(loop, kFuel);
    ASSERT_EQ(count, kFuel);
  }

  SIMPLE_TEST(PingPong) {
    int count = 0;

    auto finn = [&count]() {
      for (size_t i = 0; i < 10; ++i) {
        ++count;
        Yield();
        ASSERT_EQ(count, 0);
      }
      ++count;
    };

    auto jake = [&count]() {
      for (size_t i = 0; i < 10; ++i) {
        --count;
        Yield();
        ASSERT_EQ(count, 1);
      }
    };

    RunScheduler([&]() {
      Spawn(finn);
      Spawn(jake);
    });
  }

  SIMPLE_TEST(RoundRobin) {
    static const size_t kFibers = 5;
    static const size_t kRounds = 5;

    size_t next = 0;

    auto routine = [&](size_t k) {
      for (size_t i = 0; i < kRounds; ++i) {
        ASSERT_EQ(next, k);
        next = (next + 1) % kFibers;
        twist::fiber::Yield();
      }
    };

    RunScheduler([&]() {
      for (size_t k = 0; k < kFibers; ++k) {
        Spawn([&, k](){ routine(k); });
      }
    });
  }

  SIMPLE_TEST(Sleep) {
    static const size_t kFibers = 10;

    auto main = [&]() {
      for (size_t k = 0; k < kFibers; ++k) {
        auto routine = [k]() {
          auto sleep_duration = k * k * std::chrono::milliseconds(10);

          twist::Timer timer;
          SleepFor(sleep_duration);
          ASSERT_TRUE(timer.Elapsed() > sleep_duration);
        };

        Spawn(routine);
      }
    };

    RunScheduler(main);
  }

  /*
  void recurse() {
    recurse();
  }

  SIMPLE_TEST(StackOverflow) {
    RunScheduler(recurse);
  }
  */

  SIMPLE_TEST(ThreadLike) {
    bool done = false;

    auto child_routine = [&]() {
      for (size_t i = 0; i < 10; ++i) {
        Yield();
      }
      done = true;
    };

    RunScheduler([&]() {
      ThreadLike child(child_routine);
      ASSERT_FALSE(done);
      ThreadLike moved(std::move(child));
      moved.Join();
      ASSERT_TRUE(done);
    });
  }

  SIMPLE_TEST(TestWaitQueue) {
    WaitQueue wait_queue;
    size_t step = 0;

    auto foo = [&]() {
      ASSERT_EQ(step, 1);
      wait_queue.WakeOne();
      ASSERT_EQ(step, 1);
      ++step;
      Yield();
      ASSERT_EQ(step, 3);
    };

    auto main = [&]() {
      Spawn(foo);
      ++step;
      wait_queue.Park();
      ASSERT_EQ(step, 2);
      ++step;
      Yield();
    };

    RunScheduler(main);
  }

  SIMPLE_TEST(LiveLock) {
    // Exercise for curious reader: insert several Yield-s into this test
    // to produce deterministic livelock

    size_t count = 0;

    auto routine = [&]() {
      for (size_t i = 0; i < 3; ++i) {
        // lock
        while (count++ > 0) {
          --count;
        }

        // critical section

        // unlock
        --count;
      }
    };

    RunScheduler([&]() {
      Spawn(routine);
      Spawn(routine);
    });
  }

  SIMPLE_TEST(MutualExclusion) {
    Mutex mutex;
    bool critical = false;

    auto routine = [&]() {
      for (size_t i = 0; i < 10; ++i) {
        mutex.Lock();
        ASSERT_FALSE(critical);
        critical = true;
        for (size_t j = 0; j < 3; ++j) {
          Yield();
        }
        ASSERT_TRUE(critical);
        critical = false;
        mutex.Unlock();
        Yield();
      }
    };

    RunScheduler([&]() {
      Spawn(routine);
      Spawn(routine);
    });
  }

  SIMPLE_TEST(Futex) {
    unsigned int var = 0;

    auto waker = [&]() {
      FutexWake(&var, 1);
    };

    auto main = [&]() {
      ASSERT_EQ(FutexWait(&var, 1), -1); // returns immediately

      Spawn(waker);
      ASSERT_EQ(FutexWait(&var, 0), 0);
    };

    RunScheduler(main);
  }

  SIMPLE_TEST(LocalStorage) {
    static const size_t kFibers = 5;

    size_t key = fls::AcquireKey();

    auto routine = [&](int i) {
      int* local = (int*)fls::AccessSlot(key);
      *local = i;
      Yield();
      ASSERT_EQ(*local, i);
    };

    RunScheduler([&]() {
      for (size_t i = 0; i < kFibers; ++i) {
        Spawn([&, i]() { routine(i); });
      }
    });
  }
}
