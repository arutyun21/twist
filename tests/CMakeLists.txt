cmake_minimum_required(VERSION 3.5)

message(STATUS "Twist tests")

# List sources

set(RUN_TESTS_MAIN "./run_tests.cpp")
file(GLOB TESTS "./test*.cpp")

# Build

add_executable(twist_all_tests ${RUN_TESTS_MAIN} ${TESTS})
target_link_libraries(twist_all_tests twist)

# Run

add_custom_target(twist_run_all_tests ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/twist_all_tests)
add_dependencies(twist_run_all_tests twist_all_tests)

